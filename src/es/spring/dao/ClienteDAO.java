package es.spring.dao;

import java.util.List;

import es.spring.controlador.entity.Cliente;

public interface ClienteDAO {

	public List<Cliente> getClientes();
	
}
