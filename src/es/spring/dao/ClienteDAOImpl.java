package es.spring.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.spring.controlador.entity.Cliente;

@Repository
public class ClienteDAOImpl implements ClienteDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	@Transactional // para crear y ejecutar y hacer el commit de las transacciones 
	public List<Cliente> getClientes() {
		
		//obtener session
		Session sesion = sessionFactory.getCurrentSession();
		
		//crear consulta
		Query<Cliente> query = sesion.createQuery("from Cliente", Cliente.class);
		
		//ejecutar la query y obtener resultado 
		
		List<Cliente> listaClientes = query.getResultList();
		
		return listaClientes;
	}

}
