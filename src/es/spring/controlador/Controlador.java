package es.spring.controlador;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import es.spring.controlador.entity.Cliente;
import es.spring.dao.ClienteDAO;

@Controller
@RequestMapping("/cliente")
public class Controlador {
	
	@Autowired
	private ClienteDAO clienteDao;
	
	@RequestMapping("/lista")
	public String listaClientes(Model modelo) {
		
		//obtener los clientes desde el dao
		List<Cliente> losClientes = clienteDao.getClientes();
		
		//agregar los clientes al modelo
		modelo.addAttribute("clientes", losClientes);
		
		
		return "listaCliente";
		
	}

}
